from effects import Effect
import numpy as np
import tensorflow as tf

class HideBackground(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Hide Background"
        self.description = "Hide the background from an media"
        self.config['threshold'] = [0, 100, 75]
        self.config['onRawFrame'] = "bool"

    def do_effect(self, frame, original_frame):
        image_array = tf.keras.preprocessing.image.img_to_array(frame)
        if self.onRawFrame:
            mask = self._calculateHumanMask(original_frame, self.threshold / 100)
        else:
            mask = self._calculateHumanMask(frame, self.threshold / 100)
        return np.array(tf.keras.preprocessing.image.array_to_img(image_array * mask))