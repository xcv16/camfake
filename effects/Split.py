from effects import Effect
import numpy as np


class Split(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Split Image"
        self.description = "Split Image by the Top/Bottom/Right/Left"
        self.config["Top"] = "bool"
        self.config["Bottom"] = "bool"
        self.config["Left"] = "bool"
        self.config["Right"] = "bool"

    def do_effect(self, frame, original_frame):
        if self.Right:
            frame = np.array_split(frame, 2, axis=1)[1]
            frame = np.concatenate((np.full_like(frame, 0),frame), axis=1)
        if self.Top:
            frame = np.array_split(frame, 2)[0]
            frame = np.concatenate((frame, np.full_like(frame, 0)))
        if self.Left:
            frame = np.array_split(frame, 2, axis=1)[0]
            frame = np.concatenate((frame, np.full_like(frame, 0)), axis=1)
        if self.Bottom:
            frame = np.array_split(frame, 2)[1]
            frame = np.concatenate((np.full_like(frame, 0), frame))

        return frame
