from effects import Effect
import numpy as np
from PIL import Image
from PIL import ImageEnhance


class Brightness(Effect.Effect):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Brightness"
        self.description = "Image Brightness"
        self.config['Brightness'] = [0, 500, 100]

    def do_effect(self, frame, original_frame):
        frame = Image.fromarray(frame)
        brighter = ImageEnhance.Brightness(frame)
        frame = brighter.enhance(self.Brightness/100)
        frame = np.array(frame)
        return frame
