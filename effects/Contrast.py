from effects import Effect
import numpy as np
from PIL import Image
from PIL import ImageEnhance


class Contrast(Effect.Effect):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Contrast"
        self.description = "Contrast Image"
        self.config['Contrast'] = [0, 500, 100]

    def do_effect(self, frame, original_frame):
        frame = Image.fromarray(frame)
        contraster = ImageEnhance.Contrast(frame)
        frame = contraster.enhance(self.Contrast/100)
        frame = np.array(frame)
        return frame
