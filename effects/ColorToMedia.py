from effects import VideoEffect
import cv2
import numpy as np


class ColorToMedia(VideoEffect.VideoEffect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Color to Media"
        self.description = "Replace a color by an Image/Video"
        self.config["Color"] = "RGB"
        self.config["Source"] = "media"

    def do_effect(self, frame, original_frame):
        self.video = self.Source
        video_frame = self.getVideoFrame()
        frame_height, frame_width = frame.shape[:2]
        video_frame = cv2.resize(video_frame, (frame_width, frame_height))
        video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)
        r, c, a = np.where(frame == self.Color)
        frame[(r, c, a)] = video_frame[(r, c, a)]
        return frame

