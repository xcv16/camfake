from effects import VideoEffect
import cv2
import numpy as np


class Superpose(VideoEffect.VideoEffect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Superpose"
        self.description = "Superpose a Video/Image"
        self.config['Source'] = "media"
        self.config['Mask'] = "media"
        self.config["MaskSharpening"] = [0, 100, 50]

    def do_effect(self, frame, original_frame):
        self.video = self.Source
        self.mask = self.Mask
        superpose_frame = self.getVideoFrame()
        frame_height, frame_width = frame.shape[:2]
        image = cv2.resize(superpose_frame, (frame_width, frame_height))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        mask = self.getMaskFrame()
        mask = cv2.resize(mask, (frame_width, frame_height))
        mask = np.where(mask == 255, 1, mask)
        filter = np.array([[-1, -1, -1], [-1, 100*self.MaskSharpening, -1], [-1, -1, -1]])
        # Applying cv2.filter2D function on our Logo image
        mask = cv2.filter2D(mask, -1, filter)
        mask = np.where((mask != 1) & (mask != 0), 1, mask)
        mask = image * mask
        return np.where((mask == 0) & (frame != 0) & (frame != 1), frame, mask)
