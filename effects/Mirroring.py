from effects import Effect
import numpy as np
import cv2


class Mirroring(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Mirroring"
        self.description = "Put a mirror on left/right of the media"
        self.config["OtherSide"] = "bool"

    def do_effect(self, frame, original_frame):
        if not self.OtherSide:
            a = np.array_split(frame, 2, axis=1)[0]
        else:
            a = np.array_split(frame, 2, axis=1)[1]
        return np.concatenate((a, np.full_like(a, cv2.flip(a, 1))), axis=1)
