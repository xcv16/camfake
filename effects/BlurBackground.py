from effects import Effect
from effects import HideBackground
import cv2
import numpy as np

class BlurBackground(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Blur Background"
        self.description = "Blur the background of an media"
        self.config["blur"] = [1, 200, 80]
        self.config["threshold"] = [0, 100, 75]
        self.config['onRawFrame'] = "bool"

    def do_effect(self, frame, original_frame):
        blured_frame = cv2.blur(frame, (self.blur, self.blur))
        body = HideBackground.HideBackground()
        body.threshold = self.threshold
        body.onRawFrame = self.onRawFrame
        body = body.do_effect(frame, original_frame)
        return np.where(body == 0, blured_frame, body)