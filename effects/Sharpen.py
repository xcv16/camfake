from effects import Effect
import numpy as np
from PIL import Image
from PIL import ImageFilter


class Sharpen(Effect.Effect):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Sharpen"
        self.description = "Sharpen Image"
        self.config['Sharpen'] = [1, 10, 5]

    def do_effect(self, frame, original_frame):
        frame = Image.fromarray(frame)
        for i in range(self.Sharpen):
            frame = frame.filter(ImageFilter.SHARPEN)
        frame = np.array(frame)
        return frame
