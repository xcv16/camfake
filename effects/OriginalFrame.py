from effects import Effect


class OriginalFrame(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Original frame"
        self.description = "Return the orignala frame as send by the input device"

    def do_effect(self, frame, original_frame):
        return original_frame
