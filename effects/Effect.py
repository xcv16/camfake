from abc import abstractmethod
from tf_bodypix.api import download_model, load_model, BodyPixModelPaths
import tensorflow as tf


class Effect():
    """
    Base class for all future effect
    """
    bodypix_model = load_model(download_model(BodyPixModelPaths.MOBILENET_FLOAT_50_STRIDE_16))

    name = "Effect"
    description = "What the effect do"

    @staticmethod
    def _calculateHumanMask(frame, threshold):
        """
        Calculate mask with Bodypix model
        :param frame: a frame as a numpy array
        :param threshold: the sensibility of the model
        :return:  a mask of the displayed image body for future manipulation
        """
        _image_array = tf.keras.preprocessing.image.img_to_array(frame)
        result = Effect.bodypix_model.predict_single(_image_array)
        return result.get_mask(threshold=threshold)

    def __init__(self, *args, **kwargs):
        # Any key defined here will be morphed into an attribute and will be accessible in the do_effect methode by self.key
        self.config = {}

    @abstractmethod
    def do_effect(self, frame, original_frame):
        """
        Placeholder for future Effect
        :param frame: a frame (numpy array)
        :param original_frame: the original frame from the camera
        :return: the frame with the effect applied
        """
        pass

    def setConfig(self, key, value):
        """
        Create a new attribute or modify one
        :param key: the name of the attributes
        :param value: the attributes value
        """
        setattr(self, key, value)

    # Exist for creating an order when importing effect
    def __gt__(self, other):
        return self.name > other.name

    # Exist for creating an order when importing effect
    def __lt__(self, other):
        return self.name < other.name
