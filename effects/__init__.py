from os.path import dirname, basename, isfile, join
import glob
import importlib

modules = glob.glob(join(dirname(__file__), "*.py"))
__all__ = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
__effects__ = []
for effect in __all__:
    __effects__.append(importlib.import_module(f"effects.{effect}"))
