from effects import Effect
import numpy as np


class ColorToColor(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Color to Color"
        self.description = "Replace one color by another"
        self.config["Color1"] = "RGB"
        self.config["Color2"] = "RGB"

    def do_effect(self, frame, original_frame):
        frame[np.all(frame == self.Color1, axis=-1)] = self.Color2
        return frame
