# Effects

Here you can find all the available Effect

## Create your own effect

It's a simple a creating a python file in this directory with these constraints:

* It **MUST** inherit Effect or VideoEffect class (either you want or not use a video/image file input in your effect)
* The new effect class name **MUST** be the same as his files
* Can't Have the word "Effect" in his name
* **MUST** have a "do_effect" method with two arguments :
  * frame : the current frame after other effects
  * original_frame  : the raw frame from the input camera

Aka for the new Effect file New.py :
```python
from effects import Effect # Or VideoEffect


class New(Effect.Effect): # Or VideoEffect

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "The New displayed effect name"
        self.description = "What the Effect do"
        # Optional New effect configuration
        # The key name will be the displayed name in configuration panel **AND** the attributes name for accessing the value
        ## aka self.config["blabla"] will be accessed as self.blabla in do_effect, so beware to only use name which can represent a python variable
        ### aka no space, no beginning by number...
        self.config['Range'] = [0, 500, 100] # min, max, default
        self.config['AColor'] = "RGB" # if you need a color as input
        self.config['on/off'] = "bool" # if you nee a on/off buttons
        self.config['Media'] = "media" # if you use a image/video file as input, Need VideoEffect to work

    def do_effect(self, frame, original_frame):
        # Do something to the frame

        # return it as a numpy array        
        return frame

```

