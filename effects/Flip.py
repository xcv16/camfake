from effects import Effect
import cv2


class Flip(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Flip"
        self.description = "Make a flip to the media"
        self.config['Horizontal'] = "bool"
        self.config['Vertical'] = "bool"

    def do_effect(self, frame, original_frame):
        if self.Vertical:
            frame = cv2.flip(frame, 0)
        if self.Horizontal:
            frame = cv2.flip(frame, 1)
        return frame
