from effects import Effect
import cv2
import numpy as np


class Hologram(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Hologram"
        self.description = "Add an hologram effect to the media"

    def do_effect(self, frame, original_frame):
        # shift_img from: https://stackoverflow.com/a/53140617
        def shift_img(img, dx, dy):
            img = np.roll(img, dy, axis=0)
            img = np.roll(img, dx, axis=1)
            if dy > 0:
                img[:dy, :] = 0
            elif dy < 0:
                img[dy:, :] = 0
            if dx > 0:
                img[:, :dx] = 0
            elif dx < 0:
                img[:, dx:] = 0
            return img

        holo = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        holo = cv2.applyColorMap(holo, cv2.COLORMAP_WINTER)
        bandLength, bandGap = 2, 3

        for y in range(frame.shape[0]):
            if y % (bandLength + bandGap) < bandLength:
                holo[y, :, :] = holo[y, :, :] * np.random.uniform(0.1, 0.3)
        # the first one is roughly: holo * 0.2 + shifted_holo * 0.8 + 0
        holo2 = cv2.addWeighted(holo, 0.2, shift_img(holo.copy(), 5, 5), 0.8, 0)
        holo2 = cv2.addWeighted(holo2, 0.4, shift_img(frame.copy(), -5, -5), 0.6, 0)
        holo_done = cv2.addWeighted(frame, 0.5, holo2, 0.6, 0)

        return cv2.cvtColor(holo_done, cv2.COLOR_BGR2RGB)
