from effects import Effect
import cv2


class Greyscale(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Greyscale"
        self.description = "Make the media gray"

    def do_effect(self, frame, original_frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        gray = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
        return gray
