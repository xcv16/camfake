from effects import Effect
import numpy as np
import cv2
import random as rng


class Neoning(Effect.Effect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = "Neoning"
        self.description = "Add a Neon effect on the contour of an media, ressource intensive"
        self.config["threshold"] = [0, 100, 90]

    def do_effect(self, frame, original_frame):
        # Detect edges using Canny
        canny_output = cv2.Canny(frame, self.threshold, self.threshold * 2)
        # Find contours
        contours, hierarchy = cv2.findContours(canny_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # Draw contours
        drawing = np.zeros((canny_output.shape[0], canny_output.shape[1], 3), dtype=np.uint8)
        for i in range(len(contours)):
            color = (rng.randint(0, 256), rng.randint(0, 256), rng.randint(0, 256))
            cv2.drawContours(drawing, contours, i, color, 2, cv2.LINE_8, hierarchy, 0)

        return np.where(drawing == 0, frame, drawing)