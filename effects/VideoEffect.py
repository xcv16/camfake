from effects import Effect
import cv2
import numpy as np
from threading import Thread
import filetype
import time


class VideoEffect(Effect.Effect):
    """
    Base class for all future effect with video input
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.config['Speed'] = [0, 500, 100]
        self.videoRunning = False
        self.maskRunning = False
        self.currentVideoFrame = None
        self.currentMaskFrame = None
        self.video = None
        self.mask = None
        self.currentVideo = None
        self.currentMask = None

    def playVideo(self, type):
        """
        Play a video with open cv for getting it frame by frame
        :param type: either video or mask for determining wich video to play
        """
        while 1:
            if type == "mask":
                self.maskRunning = True
            elif type == "video":
                self.videoRunning = True

            frame = cv2.VideoCapture(getattr(self, type))
            framerate = frame.get(cv2.CAP_PROP_FPS)

            while frame.isOpened():
                speed = self.Speed
                if speed:
                    setattr(self, f"current{type.capitalize()}Frame", frame.read()[1])
                    time.sleep((1/framerate*2) / (speed / 100))
                # Reset video if not playing
                if not frame.read()[0]:
                    frame.set(cv2.CAP_PROP_POS_FRAMES, 0)
                # Change video if source change
                if getattr(self, type) != getattr(self, f"current{type.capitalize()}"):
                    setattr(self, f"current{type.capitalize()}", getattr(self, type))
                    break

    def _getFrame(self, type):
        """
        Get currently played frame
        :param type: the type of frame to get, either mask or video
        """
        if "video" in filetype.guess(getattr(self, type)).mime:
            if not getattr(self, f"{type}Running"):
                Thread(target=self.playVideo, args=(type,)).start()
        else:
            setattr(self, f"current{type.capitalize()}Frame", cv2.imread(getattr(self, type), cv2.IMREAD_COLOR))

    def getVideoFrame(self):
        """
        Return the video frame
        :return: a numpy array
        """
        self._getFrame("video")
        return self.currentVideoFrame

    def getMaskFrame(self):
        """
        Return the mask frame
        :return: a numpy array
        """
        self._getFrame("mask")
        return self.currentMaskFrame
