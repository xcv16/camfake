# CamFake

This is an attempt to create a simple UI for adding fun effect to a webcam feed for usage in another software (like firefox/anything wich can use a video feed)

/!\ Only Tested on Archlinux

# Setup

## V4l2Loopback

Having a working [v4l2loopback](https://github.com/umlaeute/v4l2loopback) setup with this parameters:
```bash
options v4l2loopback devices=2 exclusive_caps=1,1 video_nr=2,3 card_label="fake2,fake3"
```

## Dependencies 

```bash
virtualenv --python=python3 venv
source venv 
pip install -r requirements.txt
```


# Usage

```bash
source venv
python main.py
```

## UI

Choose the input camera with the dropdown, wait a few secondes for the camera to start

Then you can apply some effects with the buttons on the left. Click on one and the effect will appear in the right table.

You can add another or delete one or change in which order the effects are applied

# Credits

[Linux-Fake-Background-Webcam](https://github.com/fangfufu/Linux-Fake-Background-Webcam) For the idea 

[OpenCV tutorials](https://docs.opencv.org/master/d9/df8/tutorial_root.html) For tutorials and some image manipulation algorythms

[Bodypix](https://ml5js.org/reference/api-BodyPix/) For real time body detection in image

[python-tf-bodypix](https://github.com/de-code/python-tf-bodypix) For making Bodypix usable directly in python

[v4l2loopback](https://github.com/umlaeute/v4l2loopback) For creating virtual video devices accessible by  almost any other software

[pyfakewebcam](https://github.com/jremmons/pyfakewebcam) For writing to the virtual video devices 

[ffmpeg-python](https://github.com/kkroening/ffmpeg-python) For manipulating ffmpeg directly in python

[QDarkStyleSheet](https://github.com/ColinDuquesnoy/QDarkStyleSheet) For the base CSS style
