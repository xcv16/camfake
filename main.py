#! /usr/bin/env python3
import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog, QSizePolicy, QCheckBox, QPushButton, QHeaderView, QTableWidgetItem, QSlider, \
    QLabel, QLineEdit, QFrame, QHBoxLayout, QColorDialog
from PyQt5.QtCore import QSize, pyqtSignal, QObject, QThread, pyqtSlot, Qt
from PyQt5 import uic
from PyQt5.QtGui import QCursor, QColor
from PyQt5.QtMultimedia import QCamera, QCameraInfo, QCameraImageCapture
import ffmpeg
import time


class EffectButton(QPushButton):
    """
    A QPushButton with an Effect
    """

    def __init__(self, *args, **kwargs):
        from effects import Effect
        super().__init__(*args, **kwargs)
        self.effect = Effect.Effect


class ActionButton(QPushButton):
    """
    A QPushButton with the row index for knowing on which row doing something
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def setRow(self, row):
        """
        Set index row
        :param row:
        """
        self.row = row


class MediaButton(QPushButton):
    """
    A QPushButton with a path to a media File
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.image = ""

    def setMedia(self, media):
        """
        Set the path to a media file
        :param media: a media file
        :return: the path to image (need for some lambda function)
        """
        self.image = media
        self.setText(media)
        return media


class QCustomTableWidgetItem(QTableWidgetItem):
    """
    A QTableWidgetItem with an Effect
    """

    def __init__(self, *args, **kwargs):
        from effects import Effect
        super().__init__(*args, **kwargs)
        self.effect = Effect.Effect

    def __lt__(self, other):
        if (isinstance(other, QCustomTableWidgetItem)):
            selfDataValue = float(self.data(Qt.EditRole))
            otherDataValue = float(other.data(Qt.EditRole))
            return selfDataValue < otherDataValue
        else:
            return QtGui.QTableWidgetItem.__lt__(self, other)

    def __gt__(self, other):
        if (isinstance(other, QCustomTableWidgetItem)):
            selfDataValue = float(self.data(Qt.EditRole))
            otherDataValue = float(other.data(Qt.EditRole))
            return selfDataValue > otherDataValue
        else:
            return QtGui.QTableWidgetItem.__lt__(self, other)

    def setEffect(self, effect):
        """
        Set Effect on TableWigetItem
        :param effect: an Effect
        """
        self.effect = effect


class Camera(QObject):
    """
    An Object for manipulating all needed operations on camera (virtual or not)
    """
    finished = pyqtSignal()  # No idea why it's there

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mainWindow = None
        self.processFFMPG = []
        self.opencvEffects = []
        # video0 the source camera
        # video2 feed to opencv
        # video3 get from opencv

    def startCamera(self):
        """
        Start the selected camera in camera dropdown
        """
        comboBoxText = self.mainWindow.comboBoxSelectCamera.currentText()
        for cam in QCameraInfo.availableCameras():
            tmp = f"{cam.description()} ({cam.deviceName()})"
            if tmp == comboBoxText:
                self.spawnFFMPG(cam.deviceName(), "/dev/video2")
                time.sleep(3)
        self.finished.emit()

    def spawnFFMPG(self, input, output):
        """
        Spawn FFMPG for duplicating the real camera in a virtual one
        :param input: path to real camera (aka /dev/videoX)
        :param output: path to virtual device (aka /dev/videoX)
        """
        process = (
            ffmpeg
                .input(input, format='v4l2')
                .output(output, format="v4l2")
                .run_async(pipe_stdin=True)
        )
        process.poll()
        self.processFFMPG.append(process)

    def killFFMPG(self):
        """
        Kill all ffmpeg process when exiting
        """
        for i in self.processFFMPG:
            i.terminate()

    def applyEffects(self):
        """
        Apply all enable effect on video frame and output it to virtual output device
        """
        # import here because QThread crash otherwise
        import cv2
        import pyfakewebcam

        cap = cv2.VideoCapture(2)
        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        out = pyfakewebcam.FakeWebcam('/dev/video3', frame_width, frame_height)
        while (True):
            ret, frame = cap.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            original_frame = frame
            try:
                for effect in self.opencvEffects:
                    frame = effect.do_effect(frame, original_frame)
                out.schedule_frame(frame)
            except Exception as e:
                print(f"Can't schedule frame : {e}")


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        # import here because QThread crash otherwise
        import effects
        super().__init__(*args, **kwargs)
        self.setStyleSheet(open("dark.qss", "r").read())
        self.camThread = QThread(parent=self)
        self.camera = Camera()
        self.camera.mainWindow = self
        uic.loadUi("main.ui", self)
        # Set table geometry
        table = self.tableEffects.horizontalHeader()
        table.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        table.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        table.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        table.setSectionResizeMode(3, QHeaderView.Stretch)
        self.tableEffects.itemChanged.connect(self.sortTableEffects)
        self.closePannelEffectOption()

        # Create buttons for all available Effects
        for i in sorted([getattr(k, k.__name__.replace("effects.", ""))() for k in effects.__effects__ if
                         "Effect" not in str(k)]):
            button = EffectButton()
            # Put effect in Button
            button.effect = i
            # Different behaviour if effect has config or not
            if button.effect.config:
                button.setText(f"{button.effect.name} (settings)")
            else:
                button.setText(f"{button.effect.name}")
            button.setToolTip(f"{button.effect.description}")
            button.setCursor(QCursor(Qt.PointingHandCursor))
            button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

            # Different behaviour if effect has config or not
            if button.effect.config:
                button.clicked.connect(self.addEffectWithConfig)
            else:
                button.clicked.connect(self.addEffect)
            self.widgetEffects.layout().addWidget(button)

        # Find all available camera and put it in the dropdown
        comboBoxSelectCamera = self.comboBoxSelectCamera
        comboBoxSelectCamera.addItem("Select Camera")
        for cam in QCameraInfo.availableCameras():
            if not "fake" in cam.description().lower():
                camera = f"{cam.description()} ({cam.deviceName()})"
                comboBoxSelectCamera.addItem(camera)

        comboBoxSelectCamera.currentIndexChanged.connect(self.startInputCamera)

    def closePannelEffectOption(self):
        """
        Close the Effect configuration pannel
        """
        self.frameContainerEffectOptions.setVisible(False)
        for i in reversed(range(self.frameEffectOptions.count())):
            self.frameEffectOptions.itemAt(i).widget().setParent(None)

    def openPannelEffectOption(self):
        """
        Open the Effect configuration pannel
        """
        self.frameContainerEffectOptions.setVisible(True)
        self.frameContainerEffectOptions.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.buttonAddEffectOption.disconnect()
        self.buttonCloseEffectOption.clicked.connect(self.closePannelEffectOption)

    def addEffectWithConfig(self, existing=None):
        """
        Add all configuration to configuration pannel for an Effect
        :param existing: if the Effect already have a set config, None or an Effect
        """
        et = self.sender().effect
        # New Effect object for preventing colision between already applyed effects
        if existing:
            effect = existing
            self.buttonAddEffectOption.hide()
        else:
            effect = type(et)()
            self.buttonAddEffectOption.show()
        self.closePannelEffectOption()
        row = len(range(self.tableEffects.rowCount()))
        row += 1
        row_config = 0
        for name, value in effect.config.items():
            if existing:
                existing_value = getattr(effect, name)
            if type(value) == list:
                # Add Slider for effect with single numeric input
                if len(value) == 3:
                    textedit = QLineEdit()
                    textedit.setAlignment(Qt.AlignCenter)
                    textedit.setMaxLength(len(str(value[1])))
                    textedit.setText(str(value[2]))
                    textedit.setFixedWidth(max(10 * len(str(value[0])), 10 * len(str(value[1]))))
                    widget = QSlider(Qt.Horizontal)
                    widget.setMinimum(value[0])
                    widget.setMaximum(value[1])
                    widget.setValue(value[2])
                    widget.setSingleStep(1)
                    # the state is here for preventing QT to overwrite other argument wih a signals
                    effect.setConfig(name, value[2])
                    widget.valueChanged.connect(
                        lambda state,
                               textedit=textedit,
                               widget=widget:
                        textedit.setText(str(widget.value())))
                    widget.valueChanged.connect(
                        lambda state,
                               name=name,
                               textedit=textedit,
                               widget=widget,
                               effect=effect:
                        effect.setConfig(name, widget.value()))
                    textedit.textChanged.connect(lambda state,
                                                        textedit=textedit,
                                                        widget=widget:
                                                 widget.setValue(int(textedit.text()) if textedit.text() else 0))
                    self.frameEffectOptions.addWidget(textedit, row_config, 2)
                    if existing:
                        widget.setValue(existing_value)
            elif type(value) == str:
                # Add colorPicker for effect with color choice
                if value == "RGB":
                    widget = QColorDialog()
                    widget.setOption(QColorDialog.NoButtons)
                    widget.setOption(QColorDialog.DontUseNativeDialog)
                    effect.setConfig(name, (0, 0, 0))
                    widget.currentColorChanged.connect(
                        lambda state,
                               name=name,
                               effect=effect,
                               widget=widget:
                        effect.setConfig(name, widget.currentColor().getRgb()[:-1]))
                    if existing:
                        widget.setCurrentColor(QColor(QColor.fromRgb(*existing_value)))
                elif value == "bool":
                    # Add checkbox for effect with 'on/off' option
                    widget = QCheckBox()
                    widget.clicked.connect(lambda state,
                                                  name=name,
                                                  effect=effect,
                                                  widget=widget:
                                           effect.setConfig(name, widget.isChecked()))
                    if existing:
                        widget.setChecked(existing_value)
                    effect.setConfig(name, widget.isChecked())
                elif value == "media":
                    # Add File picker for effect with media input
                    widget = MediaButton()
                    widget.setText("Open")
                    file_dialog = QFileDialog()
                    widget.clicked.connect(
                        lambda state,
                               name=name,
                               effect=effect,
                               widget=widget,
                               file_dialog=file_dialog:
                        effect.setConfig(name, widget.setMedia(file_dialog.getOpenFileName(self)[0])))
                    if existing:
                        widget.setText(existing_value)
                    else:
                        effect.setConfig(name, "")

            self.frameEffectOptions.addWidget(QLabel(f"{name}"), row_config, 0)
            self.frameEffectOptions.addWidget(widget, row_config, 1)
            row_config += 1
        self.openPannelEffectOption()
        self.labelCurrentEffectOptionName.setText(effect.name)
        self.buttonAddEffectOption.clicked.connect(
            lambda state, effect=effect: self.addEffect(effect=effect))
        self.sortTableEffects()

    def addEffect(self, effect=None):
        """
        Add en effect
        :param effect: configured effect if needed
        """
        if not effect:
            effect = self.sender().effect
        self.closePannelEffectOption()
        position = 1
        for row in range(self.tableEffects.rowCount()):
            position = str(self.tableEffects.item(row, 0).text())
            if position == "":
                position = 1
            else:
                position = int(position)
                position += 1
        row = len(range(self.tableEffects.rowCount()))
        row += 1
        self.tableEffects.setRowCount(row)
        position = QCustomTableWidgetItem(f"{position}")
        position.setEffect(effect)
        self.tableEffects.setItem(row - 1, 0, position)
        self.tableEffects.setItem(row - 1, 2, QCustomTableWidgetItem(f"{effect.name}"))
        self.tableEffects.item(row - 1, 2).setFlags(Qt.ItemIsEnabled)
        self.tableEffects.setItem(row - 1, 3, QCustomTableWidgetItem(f"{effect.description}"))
        self.tableEffects.item(row - 1, 3).setFlags(Qt.ItemIsEnabled)
        self.sortTableEffects()

    def removeEffect(self):
        """
        Remove an effect from main interface and applied Effect
        """
        sender = self.sender()
        self.tableEffects.removeRow(sender.row)
        self.closePannelEffectOption()
        self.sortTableEffects()

    def editEffect(self):
        """
        Allow editing an already applied Effect
        """
        sender = self.sender()
        self.addEffectWithConfig(sender.effect)

    def sortTableEffects(self):
        """
        Sort the table with all efffect by their position
        """
        self.tableEffects.sortItems(0)
        self.camera.opencvEffects = []
        row = 0
        # Reapply "Edit" and "Delete" Button for consistency
        for row in range(self.tableEffects.rowCount()):
            effect = self.tableEffects.item(row, 0).effect
            frame = QFrame()
            layout = QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            frame.setLayout(layout)
            action = ActionButton("Delete")
            action.row = row
            action.clicked.connect(self.removeEffect)
            layout.addWidget(action)
            if getattr(effect, "config", None):
                edit = EffectButton("Edit")
                edit.effect = effect
                edit.clicked.connect(self.editEffect)
                layout.addWidget(edit)

            self.tableEffects.setCellWidget(row, 1, frame)
            self.camera.opencvEffects.append(effect)

    def closeInputCamera(self):
        self.camera.killFFMPG()

    def startInputCamera(self):
        self.camera.moveToThread(self.camThread)
        self.camera.finished.connect(self.startVideoWidgets)
        self.camera.finished.connect(self.camera.applyEffects)
        self.camThread.started.connect(self.camera.startCamera)
        self.camThread.start()

    def startVideoWidgets(self):
        """
        Start the two video widget and connect the video feed to it
        """
        for cam in QCameraInfo.availableCameras():
            if cam.deviceName() == "/dev/video2":
                self.originalCam = QCamera(cam)
            if cam.deviceName() == "/dev/video3":
                self.outputCam = QCamera(cam)
        self.originalCam.setCaptureMode(QCamera.CaptureVideo)
        self.originalCam.setViewfinder(self.widgetSelectedCamera)
        self.originalCam.start()
        time.sleep(0.3)  # Ugly but temporary, needed because the second widget won't start otherwise #TODO find why
        self.outputCam.setCaptureMode(QCamera.CaptureVideo)
        self.outputCam.setViewfinder(self.widgetOutputCurrent)
        self.outputCam.start()

    def closeEvent(self, event):
        """
        Close all real camera when closing the software
        :param event: ignore it, it's needed for overwriting the base method
        """
        self.closeInputCamera()


app = QtWidgets.QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec_()
